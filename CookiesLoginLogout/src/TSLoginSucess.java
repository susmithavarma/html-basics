import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TSLoginSucess")
public class TSLoginSucess extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public TSLoginSucess() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("TSUser"))
                    userName = cookie.getValue();
            }
        }
    	response.setContentType("text/html");

        if (userName == null)
            response.sendRedirect("index.html");
        PrintWriter out = response.getWriter();
        out.println(" <h3> Hi " +  userName + " , Login successful.</h3> ");
        out.println("<br><form action=\"TSLogoutServlet\" method=\"post\">" 
               + "<input type=\"submit\" value=\"Logout\">\r\n" + 
        		"        </form>\r\n" + 
        		"    </div>\r\n" + 
        		"</body>\r\n" + 
        		"</html>");
        		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}