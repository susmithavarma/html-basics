
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 * Servlet implementation class LoginServlet
 */
 
@WebServlet("/TSLoginServlet")
public class TSLoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final String userID = "TSUser";
    private final String password = "TSPassword";
 
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  String TSUser = request.getParameter("TSUser");
        String pwd = request.getParameter("TSPassword");
 
        if (userID.equals(TSUser) && password.equals(pwd)) {
            Cookie TSCookie = new Cookie("TSUser", TSUser);
            // setting cookie to expiry in 60 mins
            TSCookie.setMaxAge(60 * 60);
            response.addCookie(TSCookie);
            response.sendRedirect("TSLoginSucess");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.html");
            PrintWriter out = response.getWriter();
            out.println("<font color=red>Please make sure you enter UserID/Pass as \"TSUser : TSPassword\".</font>\n");
            rd.include(request, response);
        }
 
    }
}