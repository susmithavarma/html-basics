package studentServlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String student=request.getParameter("stuname");
		String password=request.getParameter("pass");
		try {
			//Database connection
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","123"); 		  
			System.out.println("connected");	
			//Retrieving data from database
			String query ="select rollno,stuname,pass from studentTable where stuname='"+student+"' and pass='"+password+"'";
			Statement stmt = con.createStatement(); 
			ResultSet result = stmt.executeQuery(query); 
			//checking condition
			if(result.next()==true) {
			   int studRoll = result.getInt(1);
			     	out.println("<h1>LOGIN SUCCESSFULL</h1>");
					out.println("<form action='DisplayServlet'>");
					out.println("<input type='submit' value='ok'>");
					out.println("</form>");
					HttpSession session=request.getSession();  
				    session.setAttribute("studRoll",studRoll);  
			}
			else {
				out.println("<h1>LOGIN UNSUCCESSFULL</h1>");
				out.println("<h2>please enter valid username and password!</h2>");
				out.println("<form action = /StudentLoginPage/Login.html >");
				out.println("<input type='submit' value='ok'>");
				out.println("</form>");
				//response.sendRedirect("/StudentLoginPage/Login.html"); 
				con.close();
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			out.println(e.getMessage());
			out.println("in exception");
		}
		out.close();
	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
