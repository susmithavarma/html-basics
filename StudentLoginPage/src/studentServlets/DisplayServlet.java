package studentServlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/DisplayServlet")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		HttpSession session=request.getSession(false);
		//int studRollNo= (int) session.getAttribute("studRoll");
		if(session==null)
		{
			out.println("you are not logged in! Login first!");
			response.sendRedirect("/StudentLoginPage/Login.html"); 
		    return;
			//request.getRequestDispatcher("Login.html").include(request,response);
		}
		else {
			out.println("you are logged in");
		
		int studRollNo= (int) session.getAttribute("studRoll");
		try {
			//Database connection
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","123"); 		  
			System.out.println("connected");	
			//Retrieving data from database
			String query ="select * from studentTable where rollno= "+studRollNo ;
			Statement stmt = con.createStatement(); 
			ResultSet result = stmt.executeQuery(query); 
			//checking condition
			if(result.next()==true) {
			
				//out.println("result executed");
			     out.println("student marks are: "+result.getInt(4));
					out.println("<form action='LogoutServlet'>");
					out.println("<input type='submit' value='Log Out'>");
					out.println("</form>");
					con.close();
				    
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			out.println(e.getMessage());
			out.println("in exception");
		}
		out.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
