

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class First_Filter
 */
@WebFilter(servletNames = { "Authe_Servlet" })
public class First_Filter implements Filter {

    /**
     * Default constructor. 
     */
    public First_Filter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		PrintWriter out=response.getWriter();
		String password=request.getParameter("pass");
		// pass the request along the filter chain
	      response.setContentType("text/html;charset=UTF-8");

		System.out.println("in filter");
		if(password.equals("susmitha")) {
			System.out.println("correct");
			chain.doFilter(request, response);
		
		}
		else {
			out.println("<h1>invalid username or password!</h1>");
			System.out.println("incorrect");
			RequestDispatcher rd=request.getRequestDispatcher("index.html");
			rd.include(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
