package httpSession_login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutValidation
 */
@WebServlet("/LogoutValidation")
public class LogoutValidation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutValidation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		try{  
			  
	        response.setContentType("text/html");  
	        PrintWriter out = response.getWriter();    
	        HttpSession session=request.getSession(false);  
	        String username=(String)session.getAttribute("username");  
	        String password=(String)session.getAttribute("password"); 
	        out.println("username is: "+username); 
	        out.println("password is: "+password); 
	        out.println("<form action='LogoutPage'>");
	        out.println("<input type='submit' value='Logout'>");
	        out.println("</form>");
	        out.close();  
	  
	                }catch(Exception e){System.out.println(e);}  
	    } 
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
