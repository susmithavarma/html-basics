package httpSession_login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginValidation
 */
@WebServlet("/LoginValidation")
public class LoginValidation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginValidation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
		response.setContentType("text/html");
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		PrintWriter writer=response.getWriter();
		if((username.equals("admin"))&&(password.equals("password"))) {
			
			writer.println("<h1>LOGIN SUCCESSFULL</h1>");
			writer.println("<form action='LogoutValidation'>");
			writer.println("<input type='submit' value='submit'>");
			writer.println("</form>");
			 HttpSession session=request.getSession();  
		        session.setAttribute("username",username);  
		        session.setAttribute("password",password); 
		}	
		
		else {
			writer.println("<h2>LOGIN UNSUCCESSFUL</h2>");
		}
		
	        writer.close();
		}
		catch(Exception e){System.out.println(e);}  
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
